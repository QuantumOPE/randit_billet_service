import datetime
import pyboleto
from pyboleto.bank.caixa import BoletoCaixa
from pyboleto.pdf import BoletoPDF
import os
from uuid import uuid4

data = {
    'carteira': 'SR',
    'cedente': 'RANDIT ALUGUEIS LTDA',
    'cedente_documento': "02.154.264/0001-80",
    'cedente_endereco': "Av. Rudge, 123 - Bom Retiro - Sao Paulo/SP - CEP: 12345-678",
    'agencia_cedente': '1565',
    'conta_cedente': '414-3',
    'data_vencimento': datetime.date(2020, 5, 5),
    'data_documento': datetime.date(2020, 4, 7),
    'data_processamento': datetime.date(2020, 4, 7),
    'instrucoes': [
        "- Sr Caixa, cobrar multa de 2% após o vencimento",
        "- Receber até 10 dias após o vencimento",
    ],
    'demonstrativo': [
        "- Aluguel de Furadeira de impacto 3000 R$ 65,00",
        "- Seguro contra dano/roubo/furto SP R$ 25,00",
        "- Total R$ 90,00",
    ],
    'valor_documento': 90.00,
    'nosso_numero': "8019525086",
    'numero_documento': "8019525086",
    'sacado': [
        "Ailton da Silva Vieira",
        "Rua Tito, 1000 - Vl Ipojuca - Cidade - Cep. 01234-036",
        ""
    ]
}

def gera_boleto(data):
    d = BoletoCaixa()
    d.data_fields(data)
    listaDadosCaixa = []
    listaDadosCaixa.append(d)

    boleto_arq = str(uuid4()) + '.pdf'
    boleto = BoletoPDF(boleto_arq)

    for i in range(len(listaDadosCaixa)):
        boleto.drawBoleto(listaDadosCaixa[i])
        boleto.nextPage()
    boleto.save()

    abre_boleto = open(boleto_arq, "rb+").read()
    # os.unlink(boleto_arq)
    # response.headers['Content-Type']='application/pdf'
    return boleto_arq


gera_boleto(data)
